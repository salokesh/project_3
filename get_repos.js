main();

function main() {
  let buttonRepos = document.getElementById("gitHubRepos");
  buttonRepos.addEventListener("click", fetchAndDisplayResults);
}

let divResult = document.getElementById("results");

async function fetchAndDisplayResults() {
  divResult.innerHTML = "";
  const result = await fetchResults();
  displayResult(result);
}

async function fetchResults() {
  let headers = {
    Authorization: `Token 007c459b0c9e66ede0dcbea3cd9dd8e2c21b785a`,
    accept: "application/vnd.github.mercy-preview+json"
  };

  const url = "https://api.github.com/user/repos";
  const response = await fetch(url, {
    method: "GET",
    headers: headers
  });
  const result = await response.json();

  return result;
}

function displayResult(result) {
  for (eachRepository of result) {
    divResult.appendChild(document.createElement("br"));
    const repositoryDiv = document.createElement("h3");
    repositoryDiv.setAttribute("id", eachRepository.name);
    repositoryDiv.textContent = eachRepository.name;

    repositoryDiv.addEventListener("click", fetchTopics);
    divResult.insertAdjacentHTML("beforeEnd", `${repositoryDiv.outerHTML}`);
    document
      .querySelector(`#${eachRepository.name}`)
      .addEventListener("click", fetchTopics);
  }
}

var topicsArray = "";
let repo = "";

async function fetchTopics() {

  if (document.querySelector(`.repoListContainer`)) {
    document.querySelector(`.repoListContainer`).outerHTML = "";
  }

  var repositoryName = this.textContent;

  const headers = {
    Authorization: `Token 007c459b0c9e66ede0dcbea3cd9dd8e2c21b785a`,
    accept: "application/vnd.github.mercy-preview+json"
  };

  const url = `https://api.github.com/repos/salokesh99/${repositoryName}/topics`;
  const response = await fetch(url, {
    method: "GET",
    headers: headers
  });
  const topics = await response.json();
  displayTopics(repositoryName, topics);
}


function displayTopics(repositoryName, topics) {
  topicsArray = topics.names;
  let repoList = "";

  if (topicsArray.length == 0) {
    repo = document.getElementById(repositoryName);
    repo.insertAdjacentHTML(
      "afterend",
      `<div class = "repoListContainer"><br>
      <span  class='noTopics'>No Topics Found!!!<br></span><br>
      <button id=${repositoryName}  class="${repositoryName} add">ADD TOPIC</button>
      <br><br></div>`
    );
  } 
  else {
    repo = document.getElementById(repositoryName);
    repoList = document.createElement("ul");
    repoList.setAttribute("class", "main");

    repo.insertAdjacentHTML(
      "afterEnd",
      `<div class = "repoListContainer" ><br>
      <span class = "top" >Topics of the repository are -- </span>
      <br><button id=${repositoryName} class="${repositoryName} add" >ADD TOPIC</button><br></div>`
    );

    for (eachTopic of topicsArray) {
      let topic = `<div class = "${eachTopic}Div" ><br><li>${eachTopic}</li>
      <br><button id=${eachTopic} class="${repositoryName}">DELETE</button><br><div>`;

      repoList.insertAdjacentHTML("beforeEnd", topic);
    }

    document
      .querySelector(`.${repositoryName}`)
      .insertAdjacentHTML("beforebegin", repoList.outerHTML);
    for (eachTopic of topicsArray) {
      document
        .querySelector(`#${eachTopic}`)
        .addEventListener("click", deleteTopic);
    }
  }

  document.querySelector(`.add`).addEventListener("click", addForm);
}


async function addTopics() {
  topicsArray.push(document.querySelector("#newTopic").value);
  let url = `https://api.github.com/repos/salokesh99/${repo.id}/topics`;
  update(url);
  let topicName = document.querySelector("#newTopic").value;
  if (document.querySelector(`.main`)) {
    document.querySelector(".main").insertAdjacentHTML(
      "beforeEnd",
      `<div class = "${topicName}Div" ><br><br><li>${topicName}</li>
        <br><button id=${topicName} class="${repo.id}" >DELETE</button><br></div>`
    );
  } else {
    document.querySelector(".noTopics").outerHTML = "";
    document.querySelector(`.repoListContainer`).outerHTML = "";
    let newList = `<div class = "repoListContainer" ><br>
    <span class="top" >Topics of the repository are--</span><br>
    <ul class="main" ><div class = "${topicName}Div" ><br>
    <li>${topicName}</li><br>
    <button id=${topicName} class="${repo.id}" >DELETE</button>
    <br></div></ul><button class="add"  >ADD TOPIC</button><br></div>`;
    repo.insertAdjacentHTML("afterend", newList);
    document.querySelector(`.add`).addEventListener("click", addForm);
  }

  document
    .querySelector(`#${topicName}`)
    .addEventListener("click", deleteTopic);
  if (document.querySelector(`.formDiv`)) {
    document.querySelector(`.formDiv`).outerHTML = "";
  }
}
async function deleteTopic() {
  topicsArray.splice(topicsArray.indexOf(this.id), 1);
  let url = `https://api.github.com/repos/salokesh99/${this.className}/topics`;
  update(url);

  document.querySelector(`.${this.id}Div`).outerHTML = "";
  if (document.querySelector(`.main`).hasChildNodes() == false) {
    document.querySelector(`.main`).outerHTML = "";
    document.querySelector(`.repoListContainer`).outerHTML = "";
    repo.insertAdjacentHTML(
      "afterend",
      `<div class="repoListContainer"><br>
      <span  class='noTopics'>No Topics Found!!!</span>
      <br><br><button class="add"  >ADD TOPIC</button><br></div>`
    );
    document.querySelector(`.add`).addEventListener("click", addForm);
  }
}

async function update(url) {
  let body = {
    names: topicsArray
  };

  let headers = {
    Authorization: `Token 007c459b0c9e66ede0dcbea3cd9dd8e2c21b785a`,
    accept: "application/vnd.github.mercy-preview+json"
  };

  const response = await fetch(url, {
    method: "PUT",
    headers: headers,
    body: JSON.stringify(body)
  });
  let updated_topics = await response.json();
}

let repository = "";

async function addForm() {
  repository = this.id;

  newTopicName = `
  <div class="formDiv" >
  <br><br>
  <input id="newTopic" type="text" name="TopicName" >
  <br><br>
  <button id="topicAddButton">ADD</button><br><br>
  </div>
  `;

  document.querySelector(`.add`).insertAdjacentHTML("afterEnd", newTopicName);

  document
    .querySelector("#topicAddButton")
    .addEventListener("click", addTopics);
}
